import { createECDH, createCipheriv, createDecipheriv } from "crypto"
import { Duplex } from "stream"

const ecdh = createECDH("secp521r1")
ecdh.generateKeys()
const publicKey = ecdh.getPublicKey()

export function encrypt(socket: Duplex, client: Duplex) {
    client.write(Buffer.concat([Buffer.from([publicKey.length]), publicKey]))

    let data = Buffer.alloc(0)
    client.on("data", chunk => {
        data = Buffer.concat([data, chunk])
        const len = data.readUInt8(0)
        if (data.length < len + 1) return

        const serverKey = data.slice(1, 1 + len)
        const sharedSecret = ecdh.computeSecret(serverKey).slice(0, 16)

        client.removeAllListeners("data")
        const cipher = createCipheriv("aes-128-ctr", sharedSecret, sharedSecret)
        const decipher = createDecipheriv("aes-128-ctr", sharedSecret, sharedSecret)

        client.pipe(decipher).pipe(socket)
        socket.pipe(cipher).pipe(client)

        socket.resume()
    })
}
