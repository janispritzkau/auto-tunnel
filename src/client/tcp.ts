import { createServer, connect } from "net"
import { encrypt } from "."

createServer(socket => {
    socket.pause()
    const client = connect(7890, "127.0.0.1", () => {
        socket.on("close", () => client.end())
        client.on("close", () => socket.end())

        encrypt(socket, client)
    })
}).listen(7891)