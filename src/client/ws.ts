import { createServer } from "net"
import WebSocket = require("ws")
import { Duplex } from "stream"
import { encrypt } from "."

createServer(socket => {
    socket.on("error", console.error)
    socket.pause()

    const ws = new WebSocket("ws://localhost:7890").on("open", () => {
        ws.on("close", () => socket.end())
        socket.on("end", () => ws.close())

        const duplex = new Duplex({
            write: (chunk, _enc, cb) => (ws.send(chunk), cb()),
            read(_size) {}
        })
        ws.on("message", msg => duplex.push(msg as Buffer))

        encrypt(socket, duplex)
    })

    ws.on("error", error => (console.error(error), socket.end()))
}).listen(7891)
