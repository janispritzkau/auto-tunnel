import { request, ServerResponse } from "http"
import { Socket, createServer } from "net";
import { encrypt } from "."

createServer(socket => {
    socket.pause()
    request({
        host: "127.0.0.1",
        port: 7890,
        headers: {
            "Upgrade": "tcp",
            "Connection": "Upgrade"
        }
    }).on("upgrade", (_res, client: Socket) => {
        socket.on("close", () => client.end())
        client.on("close", () => socket.end())
        encrypt(socket, client)
    }).end()
}).listen(7891)
