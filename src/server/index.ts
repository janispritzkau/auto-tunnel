import { createECDH, createDecipheriv, createCipheriv } from "crypto"
import { Duplex } from "stream"
import { connect } from "net"

const ecdh = createECDH("secp521r1")
ecdh.generateKeys()
const publicKey = ecdh.getPublicKey()

export function encrypt(socket: Duplex) {
    let data = Buffer.alloc(0)
    socket.on("data", chunk => {
        data = Buffer.concat([data, chunk])
        const len = data.readUInt8(0)
        if (data.length < len + 1) return

        const clientKey = data.slice(1, 1 + len)
        const sharedSecret = ecdh.computeSecret(clientKey).slice(0, 16)

        socket.write(Buffer.concat([Buffer.from([publicKey.length]), publicKey]))
        socket.removeAllListeners("data")

        const cipher = createCipheriv("aes-128-ctr", sharedSecret, sharedSecret)
        const decipher = createDecipheriv("aes-128-ctr", sharedSecret, sharedSecret)

        socket.pause()

        const client = connect(80, "example.com", () => {
            client.on("end", () => socket.end())
            socket.on("end", () => client.end())

            client.pipe(cipher).pipe(socket)
            socket.pipe(decipher).pipe(client)

            socket.resume()
        })
    })
}
