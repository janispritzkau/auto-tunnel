import { createServer, IncomingMessage } from "http"
import { Socket, connect } from "net"
import { encrypt } from ".";

const server = createServer((_req, res) => res.end())

server.on("upgrade", (req: IncomingMessage, socket: Socket) => {
    if (req.headers.upgrade != "tcp") return socket.end()
    socket.write("HTTP/1.1 101 Switching Protocols\r\nUpgrade: tcp\r\nConnection: Upgrade\r\n\r\n")

    encrypt(socket)
})

server.listen(7890)