import WebSocket = require("ws")
import { Duplex } from "stream"
import { encrypt } from "."

new WebSocket.Server({ port: 7890 }).on("connection", ws => {
    ws.on("error", console.error)

    const duplex = new Duplex({
        write: (chunk, _enc, cb) => (ws.send(chunk), cb()),
        read(_size) {}
    })
    ws.on("message", msg => duplex.push(msg as Buffer))

    encrypt(duplex)
})
