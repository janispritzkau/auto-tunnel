import { createServer } from "net"
import { encrypt } from "."

createServer(socket => {
    encrypt(socket)
}).listen(7890)